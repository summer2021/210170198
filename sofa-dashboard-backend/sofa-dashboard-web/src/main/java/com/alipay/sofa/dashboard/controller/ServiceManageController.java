/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alipay.sofa.dashboard.controller;

import com.alibaba.fastjson.JSONObject;
import com.alipay.sofa.common.utils.StringUtil;
import com.alipay.sofa.dashboard.cache.RegistryDataCache;
import com.alipay.sofa.dashboard.constants.SofaDashboardConstants;
import com.alipay.sofa.dashboard.domain.RpcConsumer;
import com.alipay.sofa.dashboard.domain.RpcProvider;
import com.alipay.sofa.dashboard.domain.RpcService;
import com.alipay.sofa.dashboard.model.DataCenterModel;
import com.alipay.sofa.dashboard.model.ServiceAppModel;
import com.alipay.sofa.dashboard.model.ServiceModel;
import com.alipay.sofa.rpc.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.QueryParam;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 服务治理
 *
 * @author: guolei.sgl (guolei.sgl@antfin.com) 18/12/7 下午5:05
 * @since:
 **/
@RestController
@RequestMapping("/api/service")
public class ServiceManageController {

    @Value("${com.alipay.sofa.dashboard.registry.session.address}")
    private String            SESSION_SERVER_ADDRESS;
    @Value("${com.alipay.sofa.dashboard.registry.data.address}")
    private String            DATA_SERVER_ADDRESS;
    @Value("${com.alipay.sofa.dashboard.registry.meta.address}")
    private String            META_SERVER_ADDRESS;

    @Autowired
    private RegistryDataCache registryDataCache;
    @Autowired
    private RestTemplate      restTemplate;

    @GetMapping("blacklist/update")
    public Map blacklistPush(@QueryParam("subPrefix") String subPrefix,
                             @QueryParam("pubIpList") String pubIpList) {
        JSONObject root = new JSONObject();
        JSONObject pubIp = new JSONObject();
        JSONObject subIp = new JSONObject();
        List<String> pubList = new ArrayList();
        if (StringUtils.isNotBlank(pubIpList)) {
            String[] pubIpListArr = StringUtils.split(StringUtils.trim(pubIpList), ",");
            if (pubIpListArr != null) {
                for (String s : pubIpListArr) {
                    pubList.add(s);
                }
            }
        }
        subIp.put("IP_FULL",subPrefix);
        pubIp.put("IP_FULL",pubList);
        root.put("FORBIDDEN_PUB",pubIp);
        root.put("FORBIDDEN_SUB_BY_PREFIX",subIp);
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"blacklist/update");
        ResponseEntity<Map> forEntity = restTemplate.postForEntity(httpUrl, root.toString(),Map.class);
        return forEntity.getBody();
    }

    @GetMapping("{type}/node/query")
    public Map getRegisterNodeByType(@PathVariable("type")String type) {
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"digest/"+type+"/node/query");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("persistentData/remove")
    public Map removePersistentData(@QueryParam("dataId") String dataId,
                                    @QueryParam("group") String group,
                                    @QueryParam("instanceId") String instanceId,
                                    @QueryParam("version") Long version,
                                    @QueryParam("data") String data) {
        JSONObject json = new JSONObject();
        json.put("dataId", dataId);
        json.put("group", group);
        json.put("instanceId", instanceId);
        json.put("version", version);
        json.put("data", data);
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"persistentData/remove");
        ResponseEntity<Map> forEntity = restTemplate.postForEntity(httpUrl,json, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("persistentData/put")
    public Map putPersistentData(@QueryParam("dataId") String dataId,
                                 @QueryParam("group") String group,
                                 @QueryParam("instanceId") String instanceId,
                                 @QueryParam("version") Long version,
                                 @QueryParam("data") String data) {
        JSONObject json = new JSONObject();
        json.put("dataId", dataId);
        json.put("group", group);
        json.put("instanceId", instanceId);
        json.put("version", version);
        json.put("data", data);
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"persistentData/put");
        ResponseEntity <Map> forEntity = restTemplate.postForEntity(httpUrl, json, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("manage/removePeer")
    public Map removePeer(@QueryParam("ipAddressList")String ipAddressList) {
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"manage/removePeer");
        JSONObject json = new JSONObject();
        if (StringUtils.isNotBlank(ipAddressList)) {
            String[] ipAddressArr = StringUtils.split(StringUtils.trim(ipAddressList), ",");
            if (ipAddressArr != null) {
                for (String s : ipAddressArr) {
                    json.put(s,"DefaultDataCenter");
                }
            }
        }
        ResponseEntity<Map> forEntity = restTemplate.postForEntity(httpUrl, json, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("manage/resetPeer")
    public Map resetPeer(@QueryParam("ipAddressList")String ipAddressList) {
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"manage/resetPeer");
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("ipAddressList", ipAddressList);
        ResponseEntity<Map> forEntity = restTemplate.postForEntity(httpUrl,paramMap, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("manage/changePeer")
    public Map changePeer(@QueryParam("ipAddressList")String ipAddressList) {
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"manage/changePeer");
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("ipAddressList", ipAddressList);
        ResponseEntity<Map> forEntity = restTemplate.postForEntity(httpUrl, paramMap,Map.class);
        return forEntity.getBody();
    }

    @GetMapping("manage/queryPeer")
    public List<Map<String, String>> queryPeer() {
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"manage/queryPeer");
        ResponseEntity<List> forEntity = restTemplate.getForEntity(httpUrl,List.class);
        List<String> body = forEntity.getBody();
        List<Map<String, String>> result = new ArrayList<>();
        for (String s : body) {
            Map<String, String> map = new HashMap<>();
            map.put("ipAddress",s);
            result.add(map);
        }
        return result;
    }

    @GetMapping("stopPushDataSwitch/{type}")
    public Map changeStopPushDataSwitch(@PathVariable("type") String type) {
        String inputType = type.toUpperCase();
        String httpUrl = inputType.equals("TRUE")?buildHttpUrl(META_SERVER_ADDRESS,"stopPushDataSwitch/close"):buildHttpUrl(META_SERVER_ADDRESS,"stopPushDataSwitch/open");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("renewSwitch/data/change")
    public Map changeDataRenew(@QueryParam("enable") String enable) {
        String inputType = enable.toUpperCase();
        String httpUrl = inputType.equals("TRUE")?buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/data/enable"):buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/data/disable");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("renewSwitch/session/change")
    public Map changeSessionRenew(@QueryParam("enable") String enable) {
        String inputType = enable.toUpperCase();
        String httpUrl = inputType.equals("TRUE")?buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/session/enable"):buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/session/disable");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("renewSwitch/renew/change")
    public Map changeRenew(@QueryParam("enable") String enable) {
        String inputType = enable.toUpperCase();
        String httpUrl = inputType.equals("TRUE")?buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/enable"):buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/disable");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("renewSwitch/get")
    public Map getRenewSwitch() {
        String httpUrl = buildHttpUrl(META_SERVER_ADDRESS,"renewSwitch/get");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class);
        return forEntity.getBody();
    }

    @GetMapping("datum/count")
    public String getDatumCount() {
        String httpUrl = buildHttpUrl(DATA_SERVER_ADDRESS,"digest/datum/count");
        ResponseEntity<String> forEntity = restTemplate.getForEntity(httpUrl, String.class);
        return forEntity.getBody();
    }

    @GetMapping("datum/query")
    public Map getDatumByDataInfoId(@QueryParam("dataId") String dataId,
                                    @QueryParam("group") String group,
                                    @QueryParam("instanceId") String instanceId,
                                    @QueryParam("dataCenter") String dataCenter) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("dataId", dataId);
        paramMap.put("group", group);
        paramMap.put("instanceId", instanceId);
        paramMap.put("dataCenter", dataCenter);
        String httpUrl = buildHttpUrl(DATA_SERVER_ADDRESS,"digest/datum/query?dataId={dataId}&group={group}&instanceId={instanceId}&dataCenter={dataCenter}");
        ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl, Map.class,paramMap);
        return forEntity.getBody();
    }

    @GetMapping("serverList/query")
    public List<List<String>> getServerListAll() {
        List<List<String>> response = new ArrayList<>();

        String [] httpUrl =new String []{buildHttpUrl(SESSION_SERVER_ADDRESS,"digest/session/serverList/query"),buildHttpUrl(SESSION_SERVER_ADDRESS,"digest/data/serverList/query"),buildHttpUrl(SESSION_SERVER_ADDRESS,"digest/meta/serverList/query")};

        for (int i = 0; i < 3; i++) {
            ResponseEntity<List> forEntity = restTemplate.getForEntity(httpUrl[i], List.class);
            response.add(forEntity.getBody());
        }
        return response;
    }

    @GetMapping("health/check")
    public List<Map<String,Object>> checkHealth() {
        List<Map<String,Object>> response = new ArrayList<>();
        String [] httpUrl =new String []{buildHttpUrl(SESSION_SERVER_ADDRESS,"health/check"),buildHttpUrl(DATA_SERVER_ADDRESS,"health/check"),buildHttpUrl(META_SERVER_ADDRESS,"health/check")};
        for (int i = 0; i < 3; i++) {
            ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl[i], Map.class);
            response.add(forEntity.getBody());
        }
        return response;
    }

    @GetMapping("pushSwitch")
    public List<Map> pushSwitch() {
        ArrayList<Map> response = new ArrayList<>();
        String[] httpUrl  = new String []{buildHttpUrl(SESSION_SERVER_ADDRESS,"digest/pushSwitch"),buildHttpUrl(SESSION_SERVER_ADDRESS,"digest/pushSwitch")};
        for (int i = 0; i < 2; i++) {
            ResponseEntity<Map> forEntity = restTemplate.getForEntity(httpUrl[i], Map.class);
            response.add(forEntity.getBody());
        }
        return response;
    }

    @GetMapping("/data/count")
    public List<String> getSessionDataCount() {
        String httpUrl = buildHttpUrl(SESSION_SERVER_ADDRESS,"digest/data/count");
        ResponseEntity<String> forEntity = restTemplate.getForEntity(httpUrl, String.class);
        return handleSessionDataCount(forEntity.getBody());
    }

    @GetMapping("/all-service")
    public List<ServiceModel> queryServiceListByService(@RequestParam("query") String query) {
        List<ServiceModel> data = new ArrayList<>();
        Map<String, RpcService> rpcServices = registryDataCache.fetchService();
        for (Map.Entry<String, RpcService> rpcServiceEntry : rpcServices.entrySet()) {
            final String serviceName = rpcServiceEntry.getKey();
            ServiceModel model = fetchServiceModel(serviceName);
            if (model != null && (serviceName.contains(query) || StringUtils.isBlank(query))) {
                data.add(model);
            }
        }
        System.out.println(data);
        return data;
    }

    /**
     * 获取服务列表-应用维度
     *
     * @return
     */
    @GetMapping("/all-app")
    public List<Map<String, String>> queryServiceListByApp(@RequestParam("query") String query) {
        List<Map<String, String>> data = new ArrayList<>();
        Map<String, RpcService> rpcServices = registryDataCache.fetchService();
        for (Map.Entry<String, RpcService> rpcServiceEntry : rpcServices.entrySet()) {
            final String serviceName = rpcServiceEntry.getKey();
            List<RpcProvider> providers = registryDataCache.fetchProvidersByService(serviceName);
            List<RpcConsumer> consumers = registryDataCache.fetchConsumersByService(serviceName);
            if (providers != null && providers.size() > 0) {
                providers.forEach((provider) -> {
                    if (provider.getAppName() != null && !data.contains(provider.getAppName())) {
                        Map<String, String> item = new HashMap<>();
                        item.put("appName", provider.getAppName());
                        if (provider.getAppName().contains(query) || StringUtils.isBlank(query)) {
                            data.add(item);
                        }
                    }
                });
            }
            if (consumers != null && consumers.size() > 0) {
                consumers.forEach((consumer) -> {
                    if (consumer.getAppName() != null && !data.contains(consumer.getAppName())) {
                        Map<String, String> item = new HashMap<>();
                        item.put("appName", consumer.getAppName());
                        if (consumer.getAppName().contains(query) || StringUtils.isBlank(query)) {
                            data.add(item);
                        }
                    }
                });
            }
        }
        return data;
    }

    /**
     * 查询应用的服务提供和服务消费详情
     *
     * @param appName
     * @return
     */
    @GetMapping("service-app")
    public ServiceAppModel queryServiceByAppName(@RequestParam("appName") String appName) {
        List<String> providersData = new ArrayList<>();
        List<String> consumersData = new ArrayList<>();
        ServiceAppModel result = new ServiceAppModel();
        Map<String, RpcService> rpcServices = registryDataCache.fetchService();
        for (Map.Entry<String, RpcService> rpcServiceEntry : rpcServices.entrySet()) {
            final String serviceName = rpcServiceEntry.getKey();
            List<RpcProvider> providers = registryDataCache.fetchProvidersByService(serviceName);
            List<RpcConsumer> consumers = registryDataCache.fetchConsumersByService(serviceName);
            if (providers != null && providers.size() > 0) {
                providers.forEach((provider) -> {
                    if (provider.getAppName() != null && appName.equals(provider.getAppName())
                            && !providersData.contains(serviceName)) {
                        providersData.add(serviceName);
                    }
                });
            }
            if (consumers != null && consumers.size() > 0) {
                consumers.forEach((consumer) -> {
                    if (consumer.getAppName() != null && appName.contains(consumer.getAppName())) {
                        if (consumer.getAppName() != null && appName.equals(consumer.getAppName())
                                && !consumersData.contains(serviceName)) {
                            consumersData.add(serviceName);
                        }
                    }
                });
            }
        }
        result.setConsumers(consumersData);
        result.setProviders(providersData);
        System.out.println("result" + result);
        return result;
    }

    private List<RpcProvider> fetchProviderData(String serviceName) {
        return registryDataCache.fetchProvidersByService(serviceName);
    }

    private List<RpcConsumer> fetchConsumerData(String serviceName) {
        return registryDataCache.fetchConsumersByService(serviceName);
    }

    /**
     * 获取某个服务的所有提供方
     *
     * @return
     */
    @GetMapping("query/providers")
    public List<RpcProvider> queryServiceProviders(@RequestParam("dataid") String serviceName) {
        String dataId = URLDecoder.decode(serviceName);
        return fetchProviderData(dataId);
    }

    /**
     * 获取某个服务的所有提供方
     *
     * @return
     */
    @GetMapping("query/consumers")
    public List<RpcConsumer> queryServiceConsumers(@RequestParam("dataid") String serviceName) {
        String dataId = URLDecoder.decode(serviceName);
        return fetchConsumerData(dataId);
    }

    /**
     * 获取某个服务的所有提供方
     *
     * @return
     */
    @GetMapping("query/services")
    public List<ServiceModel> queryService(@RequestParam("serviceName") String serviceName) {
        List<ServiceModel> data = new ArrayList<>();

        Map<String, RpcService> rpcServices = registryDataCache.fetchService();
        for (Map.Entry<String, RpcService> rpcServiceEntry : rpcServices.entrySet()) {
            final String currentServiceName = rpcServiceEntry.getKey();
            if (StringUtil.contains(currentServiceName, serviceName)) {
                ServiceModel model = fetchServiceModel(currentServiceName);
                data.add(model);
            }
        }

        return data;
    }

    /**
     * 模型转换
     *
     * @param serviceName
     * @return
     */
    private ServiceModel fetchServiceModel(String serviceName) {
        ServiceModel model = new ServiceModel();
        model.setServiceId(serviceName);
        String consumerNum = "0";
        String providerNum = "0";
        List<RpcConsumer> consumerSize = registryDataCache.fetchConsumersByService(serviceName);
        if (consumerSize != null) {
            consumerNum = String.valueOf(consumerSize.size());
        }
        model.setServiceConsumerAppNum(consumerNum);
        List<RpcProvider> providerSize = registryDataCache.fetchProvidersByService(serviceName);
        if (providerSize != null) {
            providerNum = String.valueOf(providerSize.size());
            Set<String> appSet = new HashSet<>();
            for (RpcProvider provider : providerSize) {
                appSet.add(provider.getAppName());
            }
            //接口本身没有app信息，所以从服务端取
            model
                .setServiceProviderAppName(StringUtils.joinWithComma(appSet.toArray(new String[0])));
        }
        model.setServiceProviderAppNum(providerNum);
        // 服务提供方和服务消费方都没有，则不展示
        if (model.getServiceConsumerAppNum().equals("0")
            && model.getServiceProviderAppNum().equals("0")) {
            return null;
        }
        return model;
    }

    public List<String> handleSessionDataCount(String str) {
        List<String> list = new ArrayList<>();
        int countIndex = 0;
        int commaIndex = 0;
        for (int i = 0; i < 2; i++) {
            countIndex = str.indexOf("count: ");
            commaIndex = str.indexOf(",");
            list.add(str.substring(countIndex + 7, commaIndex));
            str = str.substring(countIndex + 7 + (commaIndex - countIndex - 7) + 1);
        }
        countIndex = str.indexOf("count: ");
        list.add(str.substring(countIndex + 7));
        return list;
    }

    public String buildHttpUrl(String serverAddress,String mapping){
        return SofaDashboardConstants.HTTP_SCHEME+serverAddress+SofaDashboardConstants.SEPARATOR+mapping;
    }

    public DataCenterModel convertDataCenterModelFromMap(Map map) {
        DataCenterModel dataCenterModel = new DataCenterModel();
        dataCenterModel.setDefaultDataCenter((List<String>) map.get("DefaultDataCenter"));
        return dataCenterModel;
    }
}
