import React from 'react';
import { Card, Table, Divider, Input } from 'antd';
import { connect } from 'dva';
const { Search } = Input;
@connect(({ application }) => ({
  list: application.list || [],
}))
class Overview extends React.Component {
  state = {
    arr: []
  }
  getData = (keyWord) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'application/fetch',
      payload: {
        "applicationName": keyWord
      }
    });
  }

  componentDidMount() {
    this.getData("");
    let ws = new WebSocket("ws://127.0.0.1:58080/webSocket");
    if (typeof (WebSocket) == "undefined") {
      console.log("遗憾：您的浏览器不支持WebSocket");
    } else {
      console.log("恭喜：您的浏览器支持WebSocket");
      ws.onopen = (evt)=> {
        console.log("Connection open ...");
        ws.send("连接成功");
      };

      ws.onmessage = (evt)=> {
        console.log( "Received Message: " + evt.data);

        let list=eval(evt.data);
        this.setState({
          arr:list
        })

      };
      ws.onclose = (evt)=> {
        // alert(evt.data)
        console.log("Connection closed.");
        // ws.close();
      };
      ws.onerror = (evt)=> {
        console.log("error")
      };
      window.onbeforeunload = (event)=> {
        console.log("关闭WebSocket连接！");
        ws.send("关闭页面");
        event.close();
      }

    }
  }

  handleSearch = (value) => {
    this.getData(value);
  }
  render() {
    const columns = [
      {
        title: '应用名',
        dataIndex: 'applicationName',
        key: 'applicationName',
      },
      {
        title: '应用实例数',
        dataIndex: 'applicationCount',
        key: 'applicationCount',
      },
      {
        title: '操作',
        key: 'action',
        render: (text, record) => (
          <span>
            <a href={`/dashboard/instance?applicationName=${record.applicationName}`}>实例列表</a>
            <Divider type="vertical" />
            <a href="javascript:;">配置</a>
          </span>
        ),
      },
    ];

    return (
      <Card bordered={false} hoverable={false} style={{ marginTop: -15, marginLeft: -15 }}>
        <Search placeholder="input application name" onSearch={value => this.handleSearch(value)} enterButton style={{ width: 500, marginBottom: 20 }} />

        {this.props.list && (
          <Table
              dataSource={this.state.arr}
              columns={columns}
            rowKey={record => record.applicationName}
          />
        )}
      </Card>
    );
  }
};

export default Overview;