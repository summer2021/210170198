/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alipay.sofa.dashboard.listener.zookeeper;


import com.alibaba.fastjson.JSON;
import com.alipay.sofa.dashboard.configuration.NettyConfig;
import com.alipay.sofa.dashboard.spi.AppService;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author bystander
 * @version $Id: ServiceNodeChangeListener.java, v 0.1 2018年12月12日 11:25 bystander Exp $
 */
@Component
public class InstanceDetailNodeChangeListener implements PathChildrenCacheListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(InstanceDetailNodeChangeListener.class);
    @Autowired
    private AppService appService;

    @Override
    public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
        // 解决自动重连情况下出现的空指针问题
        ChildData data = event.getData();
        if (data == null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("event type : {}", event.getType());
            }
            return;
        }

        final String path = data.getPath();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("provider : {}", path);
        }


        //无论是节点 新增、删除、更新都进行数据的推送
        String result = JSON.toJSONString(appService.getAllStatistics());
        LOGGER.debug("add data :{}",result);
        NettyConfig.getChannelGroup().writeAndFlush(new TextWebSocketFrame(result));


    }
}