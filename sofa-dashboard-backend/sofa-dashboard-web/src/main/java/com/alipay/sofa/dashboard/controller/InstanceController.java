/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alipay.sofa.dashboard.controller;

import com.alipay.sofa.dashboard.response.ResponseCache;
import com.alipay.sofa.dashboard.client.model.common.HostAndPort;
import com.alipay.sofa.dashboard.client.model.env.EnvironmentDescriptor;
import com.alipay.sofa.dashboard.client.model.env.PropertySourceDescriptor;
import com.alipay.sofa.dashboard.client.model.health.HealthDescriptor;
import com.alipay.sofa.dashboard.client.model.info.InfoDescriptor;
import com.alipay.sofa.dashboard.client.model.logger.LoggersDescriptor;
import com.alipay.sofa.dashboard.client.model.mappings.MappingsDescriptor;
import com.alipay.sofa.dashboard.client.model.memory.MemoryDescriptor;
import com.alipay.sofa.dashboard.client.model.thread.ThreadSummaryDescriptor;
import com.alipay.sofa.dashboard.model.*;
import com.alipay.sofa.dashboard.spi.AppService;
import com.alipay.sofa.dashboard.spi.MonitorService;
import com.alipay.sofa.dashboard.utils.HostPortUtils;
import com.alipay.sofa.dashboard.utils.MapUtils;
import com.alipay.sofa.dashboard.utils.TreeNodeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 应用实例控制器
 *
 * @author guolei.sgl (guolei.sgl@antfin.com) 2019/7/11 2:51 PM
 * @author chen.pengzhi (chpengzh@foxmail.com)
 */
@RestController
@RequestMapping("/api/instance")
public class InstanceController {

    @Autowired
    private AppService     applicationService;

    @Autowired
    private MonitorService service;

    @Autowired
    private RestTemplate   restTemplate;

    @Autowired
    private ResponseCache  responseCache;

    @GetMapping
    public List<InstanceRecord> instances(
        @RequestParam(value = "applicationName", required = false) String applicationName) {
        // 这里默认情况如果没有传入 applicationName，前端应不展示任何数据
        if (StringUtils.isEmpty(applicationName)){
            return new ArrayList<>();
        }
        return applicationService.getInstancesByName(applicationName).stream()
            .map(InstanceRecord::new)
            .collect(Collectors.toList());
    }

    @GetMapping("/{instanceId}/env")
    public RecordResponse getEnv(
        @PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        EnvironmentDescriptor descriptor = service.fetchEnvironment(hostAndPort);

        //
        // 注意descriptor可能为空
        //
        Map<String, String> envMap = new HashMap<>();
        for (PropertySourceDescriptor propertySource :
            Optional.ofNullable(descriptor).orElse(new EnvironmentDescriptor())
                .getPropertySources()) {
            propertySource.getProperties().forEach((key, value) ->
                envMap.put(key, String.valueOf(value.getValue())));
        }

        //
        // 接口层重新拼装一次前端需要的数据结构概览
        //
        return RecordResponse.newBuilder()
            .overview("Name", envMap.get("spring.application.name"))
            .overview("Address",
                String.format("%s:%d", hostAndPort.getHost(), hostAndPort.getPort()))
            .overview("sofa-boot.version", envMap.get("sofa-boot.version"))
            .detail(TreeNodeConverter.convert(descriptor))
            .build();
    }

    @GetMapping("/{instanceId}/info")
    public RecordResponse getInfo(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        InfoDescriptor descriptor = service.fetchInfo(hostAndPort);

        //
        // 注意descriptor可能为空
        //
        Map<String, Object> infoMap = MapUtils.toFlatMap(
            Optional.ofNullable(descriptor).orElse(new InfoDescriptor()).getInfo())
            .entrySet().stream()
            .limit(3)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        //
        // 接口层重新拼装一次前端需要的数据结构概览
        //
        return RecordResponse.newBuilder()
            .overview(infoMap)
            .detail(TreeNodeConverter.convert(descriptor))
            .build();
    }

    @GetMapping("/{instanceId}/health")
    public RecordResponse getHealth(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        HealthDescriptor descriptor = service.fetchHealth(hostAndPort);

        //
        // 注意descriptor可能为空
        //
        if (descriptor == null) {
            descriptor = new HealthDescriptor();
            descriptor.setStatus("UNKNOWN");
        }

        return RecordResponse
            .newBuilder()
            .overview("Health", descriptor.getStatus())
            .overview("Address",
                String.format("%s:%d", hostAndPort.getHost(), hostAndPort.getPort()))
            .detail(TreeNodeConverter.convert(descriptor)).build();
    }

    @GetMapping("/{instanceId}/loggers")
    public RecordResponse getLoggers(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        LoggersDescriptor descriptor = service.fetchLoggers(hostAndPort);

        return RecordResponse.newBuilder().overview(new HashMap<>(16))
            .detail(TreeNodeConverter.convert(descriptor)).build();
    }

    @GetMapping("/{instanceId}/mappings")
    public RecordResponse getMappings(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        MappingsDescriptor descriptor = service.fetchMappings(hostAndPort);

        int servletCount = descriptor == null ? 0 : descriptor.getMappings().values()
            .stream()
            .map(it -> it.getServlets().size())
            .reduce(Integer::sum)
            .orElse(0);
        int servletFilterCount = descriptor == null ? 0 : descriptor.getMappings().values()
            .stream()
            .map(it -> it.getServletFilters().size())
            .reduce(Integer::sum)
            .orElse(0);
        int dispatcherServletCount = descriptor == null ? 0 : descriptor.getMappings().values()
            .stream()
            .map(it -> it.getDispatcherServlet().size())
            .reduce(Integer::sum)
            .orElse(0);

        return RecordResponse.newBuilder()
            .overview("DispatcherServletCount", String.valueOf(dispatcherServletCount))
            .overview("ServletFilterCount", String.valueOf(servletFilterCount))
            .overview("ServletCount", String.valueOf(servletCount))
            .detail(TreeNodeConverter.convert(descriptor))
            .build();
    }

    @GetMapping("/{instanceId}/memory")
    public List<StampedValueEntity<MemoryDescriptor>> getMemoryRecords(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        return service.fetchMemoryInfo(hostAndPort);
    }

    @GetMapping("/{instanceId}/thread")
    public List<StampedValueEntity<ThreadSummaryDescriptor>> getThreadRecords(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        return service.fetchThreadInfo(hostAndPort);
    }


    /**
     * 获取调用记录
     * @param instanceId
     * @return
     * @author hongminfeng
     */
    @GetMapping("/{instanceId}/httpTrace")
    public List<HttpTraceVO> fetchHttpTrace(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        String url = "http://" + hostAndPort.getHost() + ":" + hostAndPort.getPort() + "/actuator/httptrace";
        ResponseEntity<HttpTrace> forEntity = restTemplate.getForEntity(url, HttpTrace.class);
        List<HttpTraceVO> httpTraceVOList = new ArrayList<>();

        HttpTrace jsonObject = forEntity.getBody();
        for (HttpTrace.TracesDTO t : jsonObject.getTraces()) {
            HttpTraceVO object = new HttpTraceVO();

            object.setTimestamp(t.getTimestamp());
            object.setMethod(t.getRequest().getMethod());
            object.setUrl(t.getRequest().getUri());
            object.setStatus(t.getResponse().getStatus());

            //如果状态码是不是200，则不会有Date属性
            object.setDate(t.getResponse().getHeaders().getDate() == null ? null : t.getResponse().getHeaders().getDate().get(0));
            httpTraceVOList.add(object);

        }


        return httpTraceVOList;
    }


    /**
     * 获取响应状态码记录
     * @param instanceId
     * @return
     */
    @GetMapping("/{instanceId}/responseStatus")
    public List<HttpTraceVO.BizChartsObject> fetchResponseStatus(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        String url = "http://" + hostAndPort.getHost() + ":" + hostAndPort.getPort() + "/actuator/httptrace";
        ResponseEntity<HttpTrace> forEntity = restTemplate.getForEntity(url, HttpTrace.class);

        int Count_200 = 0;
        int Count_300 = 0;
        int Count_400 = 0;
        int Count_500 = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");


        HttpTrace jsonObject = forEntity.getBody();
        HttpTraceVO.BizChartsObject bizChartsObject_200 = new HttpTraceVO.BizChartsObject();
        HttpTraceVO.BizChartsObject bizChartsObject_300 = new HttpTraceVO.BizChartsObject();
        HttpTraceVO.BizChartsObject bizChartsObject_400 = new HttpTraceVO.BizChartsObject();
        HttpTraceVO.BizChartsObject bizChartsObject_500 = new HttpTraceVO.BizChartsObject();
        for (HttpTrace.TracesDTO t : jsonObject.getTraces()) {

            if(t.getResponse().getStatus()>=200 && t.getResponse().getStatus()<300)
            {
                Count_200+=1;

            }else if(t.getResponse().getStatus()>=300 && t.getResponse().getStatus()<400)
            {
                Count_300+=1;

            }else if(t.getResponse().getStatus()>=400 && t.getResponse().getStatus()<500)
            {
                Count_400+=1;

            }else if(t.getResponse().getStatus()>=500 && t.getResponse().getStatus()<600)
            {
                Count_500+=1;

            }

            bizChartsObject_200.setBizStatus("200");
            bizChartsObject_200.setBizDate(simpleDateFormat.format(new Date()));
            bizChartsObject_200.setBizCount(Count_200);

            bizChartsObject_300.setBizStatus("300");
            bizChartsObject_300.setBizDate(simpleDateFormat.format(new Date()));
            bizChartsObject_300.setBizCount(Count_300);

            bizChartsObject_400.setBizStatus("400");
            bizChartsObject_400.setBizDate(simpleDateFormat.format(new Date()));
            bizChartsObject_400.setBizCount(Count_400);

            bizChartsObject_500.setBizStatus("500");
            bizChartsObject_500.setBizDate(simpleDateFormat.format(new Date()));
            bizChartsObject_500.setBizCount(Count_500);

        }
        responseCache.fetchBizChartsList().add(bizChartsObject_200);
        responseCache.fetchBizChartsList().add(bizChartsObject_300);
        responseCache.fetchBizChartsList().add(bizChartsObject_400);
        responseCache.fetchBizChartsList().add(bizChartsObject_500);

        return responseCache.fetchBizChartsList();
    }

    /**
     * 获取运行日志
     * @param instanceId
     * @return
     */
    @GetMapping("/{instanceId}/logFile")
    public String fetchLogFile(@PathVariable("instanceId") String instanceId) {
        HostAndPort hostAndPort = HostPortUtils.getById(instanceId);
        String url = "http://" + hostAndPort.getHost() + ":" + hostAndPort.getPort() + "/actuator/logfile";
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);

        String jsonObject = forEntity.getBody();

        return jsonObject;
    }
}
