package com.alipay.sofa.dashboard.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.List;

public class HttpTrace {

    @JsonProperty("traces")
    private List<TracesDTO> traces;

    public List<TracesDTO> getTraces() {
        return traces;
    }

    public void setTraces(List<TracesDTO> traces) {
        this.traces = traces;
    }

    public static class TracesDTO {
        @JsonProperty("timestamp")
        private String timestamp;
        @JsonProperty("principal")
        private Object principal;
        @JsonProperty("session")
        private Object session;
        @JsonProperty("request")
        private RequestDTO request;
        @JsonProperty("response")
        private ResponseDTO response;
        @JsonProperty("timeTaken")
        private Integer timeTaken;

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public Object getPrincipal() {
            return principal;
        }

        public void setPrincipal(Object principal) {
            this.principal = principal;
        }

        public Object getSession() {
            return session;
        }

        public void setSession(Object session) {
            this.session = session;
        }

        public RequestDTO getRequest() {
            return request;
        }

        public void setRequest(RequestDTO request) {
            this.request = request;
        }

        public ResponseDTO getResponse() {
            return response;
        }

        public void setResponse(ResponseDTO response) {
            this.response = response;
        }

        public Integer getTimeTaken() {
            return timeTaken;
        }

        public void setTimeTaken(Integer timeTaken) {
            this.timeTaken = timeTaken;
        }

        public static class RequestDTO {
            @JsonProperty("method")
            private String method;
            @JsonProperty("uri")
            private String uri;
            @JsonProperty("headers")
            private HeadersDTO headers;
            @JsonProperty("remoteAddress")
            private Object remoteAddress;

            public String getMethod() {
                return method;
            }

            public void setMethod(String method) {
                this.method = method;
            }

            public String getUri() {
                return uri;
            }

            public void setUri(String uri) {
                this.uri = uri;
            }

            public HeadersDTO getHeaders() {
                return headers;
            }

            public void setHeaders(HeadersDTO headers) {
                this.headers = headers;
            }

            public Object getRemoteAddress() {
                return remoteAddress;
            }

            public void setRemoteAddress(Object remoteAddress) {
                this.remoteAddress = remoteAddress;
            }


            public static class HeadersDTO {
                @JsonProperty("host")
                private List<String> host;
                @JsonProperty("accept-encoding")
                private List<String> acceptencoding;
                @JsonProperty("accept")
                private List<String> accept;
                @JsonProperty("user-agent")
                private List<String> useragent;

                public List<String> getHost() {
                    return host;
                }

                public void setHost(List<String> host) {
                    this.host = host;
                }

                public List<String> getAcceptencoding() {
                    return acceptencoding;
                }

                public void setAcceptencoding(List<String> acceptencoding) {
                    this.acceptencoding = acceptencoding;
                }

                public List<String> getAccept() {
                    return accept;
                }

                public void setAccept(List<String> accept) {
                    this.accept = accept;
                }

                public List<String> getUseragent() {
                    return useragent;
                }

                public void setUseragent(List<String> useragent) {
                    this.useragent = useragent;
                }
            }
        }


        public static class ResponseDTO {
            @JsonProperty("status")
            private Integer status;
            @JsonProperty("headers")
            private HeadersDTO headers;

            public Integer getStatus() {
                return status;
            }

            public void setStatus(Integer status) {
                this.status = status;
            }

            public HeadersDTO getHeaders() {
                return headers;
            }

            public void setHeaders(HeadersDTO headers) {
                this.headers = headers;
            }

            public static class HeadersDTO {
                @JsonProperty("Transfer-Encoding")
                private List<String> transferEncoding;
                @JsonProperty("Date")
                private List<String> date;
                @JsonProperty("Content-Type")
                private List<String> contentType;

                public List<String> getTransferEncoding() {
                    return transferEncoding;
                }

                public void setTransferEncoding(List<String> transferEncoding) {
                    this.transferEncoding = transferEncoding;
                }

                public List<String> getDate() {
                    return date;
                }

                public void setDate(List<String> date) {
                    this.date = date;
                }

                public List<String> getContentType() {
                    return contentType;
                }

                public void setContentType(List<String> contentType) {
                    this.contentType = contentType;
                }
            }
        }
    }
}
