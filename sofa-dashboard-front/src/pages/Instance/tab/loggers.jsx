import React from "react";
import {connect} from "dva";
import PropsMonitor from '../component/PropsMonitor';
import PropertyDetail from "../component/PropertyDetail";
import { Input } from 'antd';
const { TextArea } = Input;

/**
 * 应用信息展示控制台
 */
@connect(({monitor}) => ({
  monitor: monitor,
}))
class Loggers extends React.Component {

  componentDidMount() {
    this.load('fetchLoggers');

    const intervalId = setInterval(() => {
      this.load('fetchLoggers');

    }, 5000);
    this.setState({intervalId: intervalId})
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    clearInterval(this.state.intervalId);
  }

  render() {
    return (
      <div>
        <TextArea
            value={this.props.monitor.loggers === undefined ? [] : this.props.monitor.loggers}
            autoSize={{ minRows: 3, maxRows: 30 }}
        />
      </div>
    )
  };

  load = (type) => {
    const {dispatch} = this.props;
    dispatch({
      type: 'monitor/' + type,
      payload: {
        instanceId: this.props.id
      }
    })
  };
}


export default Loggers;
