package com.alipay.sofa.dashboard.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DataCenterModel {


    @JsonProperty("DefaultDataCenter")
    private List<String> defaultDataCenter;

    public List<String> getDefaultDataCenter() {
        return defaultDataCenter;
    }

    public void setDefaultDataCenter(List<String> defaultDataCenter) {
        this.defaultDataCenter = defaultDataCenter;
    }

    @Override
    public String toString() {
        return "DataCenterModel{" +
                "defaultDataCenter=" + defaultDataCenter +
                '}';
    }
}