import {queryBlacklistPush, queryRegisterNodeByType, queryPeerList,queryPersistentDataRemove,queryPersistentDataPut,queryResetPeer,queryChangePeer,queryRemovePeer,changeStopPushDataSwitch,changeRenew,changeSessionRenew,changeDataRenew,getRenewSwitch, getDatumCount,getDatumByDataInfoId,queryServerListAll,checkHealth,getSessionDataCount,getPushSwitch,queryAll, queryServiceByAppName, queryProviderDetails, queryConsumerDetails } from '../services/governance';

const GovernanceModel = {
    namespace: 'governance',
    state: {
        list: [],
        providerListData: [],
        consumerListData: [],
        providerDetail: [],
        consumerDetail: [],
        sessionHealth:[],
        metaHealth:[],
        dataHealth:[],
        sessionServerList: [],
        dataServerList: [],
        metaServerList: [],
        datum:[],
        datumCount:"",
        sessionPushSwitch:[],
        metaPushSwitch:[],
        enableDataDatumExpire:"",
        enableDataRenewSnapshot:"",
        renewEnable:[],
        sessionRenewEnable:[],
        dataRenewEnable:[],
        peerList:[],
        registerNodeList:[]
    },
    reducers: {
        restateForHealth(state, action) {
            return {
                ...state,
                sessionHealth: action.payload[0],
                metaHealth: action.payload[2],
                dataHealth: action.payload[1],
            }
        },
        restateForRegisterNodeByType(state, action) {
            return {
                ...state,
                registerNodeList: action.payload,
            }
        },
        restateForPeerList(state, action) {
            return {
                ...state,
                peerList: action.payload,
            }
        },
        restateForRenewPushSwitch(state, action) {
            return {
                ...state,
                enableDataDatumExpire: action.payload.enableDataDatumExpire,
                enableDataRenewSnapshot: action.payload.enableDataRenewSnapshot,
            }
        },
        restateForDataRenew(state, action) {
            return {
                ...state,
                dataRenewEnable: action.payload.success,
            }
        },

        restateForRenew(state, action) {
            return {
                ...state,
                renewEnable: action.payload.success,
            }
        },
        restateForSessionRenew(state, action) {
            return {
                ...state,
                sessionRenewEnable: action.payload.success,
            }
        },
        restateForDatumCount(state, action) {
            return {
                ...state,
                datumCount: action.payload
            }
        },
        restateForDatumByDataInfoId(state, action) {
            return {
                ...state,
                datum: action.payload
            }
        },

        restateForDataCenterList(state, action) {
            return {
                ...state,
                dataCenterList: action.payload
            }
        },

        restateForServerList(state, action) {
            return {
                ...state,
                sessionServerList: action.payload[0],
                dataServerList: action.payload[1],
                metaServerList: action.payload[2],
            }
        },

        restateForPushSwitch(state, action) {
            return {
                ...state,
                sessionPushSwitch: action.payload[0].pushSwitch,
                metaPushSwitch: action.payload[1].pushSwitch,

            }
        },
        restate(state, action) {
            return {
                ...state,
                list: action.payload
            }
        },

        restateForDrawer(state, action) {
            return {
                ...state,
                providerListData: action.payload.providers,
                consumerListData: action.payload.consumers
            }
        },

        restateForProviderDetails(state, action) {
            return {
                ...state,
                providerDetail: action.payload,
            }
        },

        restateForConsumerDetails(state, action) {
            return {
                ...state,
                consumerDetail: action.payload,
            }
        },
    },
    effects: {
        *fetchBlacklistPush({ payload }, { call }) {
            const response = yield call(queryBlacklistPush, payload);
            return response
        },
        *fetchRegisterNodeByType({ payload }, { call,put }) {
            const response = yield call(queryRegisterNodeByType, payload);
            yield put({
                type: 'restateForRegisterNodeByType',
                payload: response,
            });
        },
        *fetchPersistentDataRemove({ payload }, { call }) {
            const response = yield call(queryPersistentDataRemove, payload);
            return response
        },
        *fetchPersistentDataPut({ payload }, { call }) {
            const response = yield call(queryPersistentDataPut, payload);
            return response
        },
        *fetchChangePeer({ payload }, { call, put }) {
            const response = yield call(queryChangePeer, payload);
            return response
        },
        *fetchResetPeer({ payload }, { call, put }) {
            const response = yield call(queryResetPeer, payload);
            return response
        },
        *fetchRemovePeer({ payload }, { call, put }) {
            const response = yield call(queryRemovePeer, payload);
            return response
        },
        *fetchPeerList({ payload }, { call ,put}) {
            const response = yield call(queryPeerList, payload);
            yield put({
                type: 'restateForPeerList',
                payload: response,
            });
        },
        *fetchStopPushDataSwitch({ payload }, { call, put }) {
            const response = yield call(changeStopPushDataSwitch, payload);

        },
        *fetchRenewPushSwitch({ payload }, { call, put }) {
            const response = yield call(getRenewSwitch, payload);
            yield put({
                type: 'restateForRenewPushSwitch',
                payload: response,
            });
        },
        *fetchDataRenew({ payload }, { call, put }) {
            const response = yield call(changeDataRenew, payload);
            yield put({
                type: 'restateForDataRenew',
                payload: response,
            });
        },
        *fetchRenew({ payload }, { call, put }) {
            const response = yield call(changeRenew, payload);
            yield put({
                type: 'restateForRenew',
                payload: response,
            });
        },
        *fetchSessionRenew({ payload }, { call, put }) {
            const response = yield call(changeSessionRenew, payload);
            yield put({
                type: 'restateForSessionRenew',
                payload: response,
            });
        },
        *fetchDatumCount({ payload }, { call, put }) {
            const response = yield call(getDatumCount, payload);
            yield put({
                type: 'restateForDatumCount',
                payload: response,
            });
        },
        *fetchDatumByDataInfoId({ payload }, { call, put }) {
            const response = yield call(getDatumByDataInfoId, payload);
            yield put({
                type: 'restateForDatumByDataInfoId',
                payload: response,
            });
            return response
        },
        *fetchServerListAll({ payload }, { call,put }) {
            const response = yield call(queryServerListAll, payload);
            yield put({
                type: 'restateForServerList',
                payload: response,
            });
        },
        *fetchHealth({ payload }, { call ,put}) {
            const response = yield call(checkHealth, payload);
            yield put({
                type: 'restateForHealth',
                payload: response,
            });
        },
        *fetchSessionDataCount({ payload }, { call }) {
            const response = yield call(getSessionDataCount, payload);
            return response;
        },
        *fetchPushSwitch({ payload }, { call , put}) {
            const response = yield call(getPushSwitch, payload);
            yield put({
                type: 'restateForPushSwitch',
                payload: response,
            });
        },
        *fetch({ payload }, { call, put }) {
            const response = yield call(queryAll, payload);
            yield put({
                type: 'restate',
                payload: response,
            });
        },

        *fetchServiceByMatch({ payload }, { call, put }) {
            const response = yield call(fetchServiceByMatch, payload);
            yield put({
                type: 'restate',
                payload: response,
            });
        },

        *fetchServiceByAppName({ payload }, { call, put }) {
            const response = yield call(queryServiceByAppName, payload);
            yield put({
                type: 'restateForDrawer',
                payload: response,
            });
        },


        *fetchProviderDetails({ payload }, { call, put }) {
            const response = yield call(queryProviderDetails, payload);
            yield put({
                type: 'restateForProviderDetails',
                payload: response,
            });
        },

        *fetchConsumerDetails({ payload }, { call, put }) {
            const response = yield call(queryConsumerDetails, payload);
            yield put({
                type: 'restateForConsumerDetails',
                payload: response,
            });
        },
    },
};

export default GovernanceModel;
