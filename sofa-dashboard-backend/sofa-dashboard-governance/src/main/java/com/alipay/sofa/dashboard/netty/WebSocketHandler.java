package com.alipay.sofa.dashboard.netty;

import com.alibaba.fastjson.JSON;
import com.alipay.sofa.dashboard.configuration.NettyConfig;
import com.alipay.sofa.dashboard.spi.AppService;
import io.netty.channel.*;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;


/**
 * 操作执行类
 * <p>
 * TextWebSocketFrame类型,表示一个文本帧
 *
 * @author Tom
 * @date 2020-08-06
 */
@Component
@ChannelHandler.Sharable
public class WebSocketHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    private static final Logger log = LoggerFactory.getLogger(WebSocketHandler.class);
    @Autowired
    private AppService appService;

    /**
     * 一旦连接,第一个被执行
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        NettyConfig.getChannelGroup().add(ctx.channel());
        String result = JSON.toJSONString(appService.getAllStatistics());
        ctx.channel().writeAndFlush(new TextWebSocketFrame(result));
    }

    /**
     * 读取数据
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {

        InetSocketAddress ipSocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String address = ipSocket.getAddress().getHostAddress();
        String result = JSON.toJSONString(appService.getAllStatistics());

        NettyConfig.getChannelGroup().writeAndFlush(new TextWebSocketFrame(result));

    }

    /**
     * 移除通道及关联用户
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        NettyConfig.getChannelGroup().remove(ctx.channel());
    }

    /**
     * 异常处理
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.info("异常：{}", cause.getMessage());
        NettyConfig.getChannelGroup().remove(ctx.channel());
        ctx.close();
    }


    /**
     * 心跳检测相关方法 - 会主动调用handlerRemoved
     *
     * @param ctx
     * @param evt
     * @throws Exception
     */
    @Override
    public void userEventTriggered(final ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state() == IdleState.ALL_IDLE) {
                //清除超时会话
                ChannelFuture writeAndFlush = ctx.writeAndFlush("you will close");
                writeAndFlush.addListener(new ChannelFutureListener() {

                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        ctx.channel().close();
                    }
                });
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }


}
