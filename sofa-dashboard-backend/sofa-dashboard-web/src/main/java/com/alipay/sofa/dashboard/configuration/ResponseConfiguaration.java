package com.alipay.sofa.dashboard.configuration;

import com.alipay.sofa.dashboard.response.ResponseCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResponseConfiguaration {
    @Bean
    public ResponseCache responseCache() {
        return new ResponseCache();
    }
}
