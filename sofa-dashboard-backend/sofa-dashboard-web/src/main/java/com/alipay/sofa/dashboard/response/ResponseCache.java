package com.alipay.sofa.dashboard.response;

import com.alipay.sofa.dashboard.model.HttpTraceVO;


import java.util.ArrayList;
import java.util.List;


public class ResponseCache {
    private List<HttpTraceVO.BizChartsObject> bizChartsObjects = new ArrayList<>();

   public List<HttpTraceVO.BizChartsObject> fetchBizChartsList()
   {
       return this.bizChartsObjects;
   }

    public List<HttpTraceVO.BizChartsObject> getBizChartsObjects() {
        return bizChartsObjects;
    }

    public void setBizChartsObjects(List<HttpTraceVO.BizChartsObject> bizChartsObjects) {
        this.bizChartsObjects = bizChartsObjects;
    }
}
