import React from "react";
import {connect} from "dva";
import {Table, Tag} from 'antd';
import {Chart, LineAdvance} from "bizcharts";

/**
 * 应用信息展示控制台
 */
@connect(({monitor}) => ({
    monitor: monitor,
}))
class HttpTrace extends React.Component {

    componentDidMount() {
        this.load('fetchHttpTrace');
        this.load('fetchResponseStatus');

        const intervalId = setInterval(() => {
            this.load('fetchHttpTrace');
            this.load('fetchResponseStatus');

            console.log(this.props.monitor)
        }, 5000);
        this.setState({intervalId: intervalId})
    }

    componentWillUnmount() {
        // use intervalId from the state to clear the interval
        clearInterval(this.state.intervalId);
    }

    render() {

        const columns = [
            {
                title: 'Timestamp',
                dataIndex: 'timestamp',
                key:'timestamp'
            },
            {
                title: 'Method',
                dataIndex: 'method',
                key: 'method',
                render(method) {
                    let methodColor = 'green'
                    if (method === 'GET') {
                        methodColor = 'green'
                    } else if (method === 'POST') {
                        methodColor = 'blue'
                    } else if (method === 'DELETE') {
                        methodColor = 'volcano'
                    }
                    return (
                        <Tag color={methodColor}>
                            {method}
                        </Tag>
                    )
                }
            },
            {
                title: 'Url',
                dataIndex: 'url',
                key:'url'
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key:'status',
                render(status) {
                    let statusColor = 'green'
                    if (status >= 200 && status < 300) {
                        statusColor = 'green'
                    } else if (status >= 300 && status < 400) {
                        statusColor = 'blue'
                    } else if (status >= 400 && status < 500) {
                        statusColor = 'gold'
                    } else if (status >= 500 && status < 600) {
                        statusColor = 'red'
                    }
                    return (
                        <Tag color={statusColor}>
                            {status}
                        </Tag>
                    )
                }

            }
        ];


        return (
            <div>
                <Chart padding={[10, 20, 50, 40]} autoFit height={300} data={this.props.monitor.responseStatus === undefined ? [] : this.props.monitor.responseStatus} >
                    <LineAdvance
                        shape="smooth"
                        point
                        area
                        position="bizDate*bizCount"
                        color="bizStatus"
                    />

                </Chart>
                <Table columns={columns}
                       dataSource={this.props.monitor.httpTrace === undefined ? [] : this.props.monitor.httpTrace}/>
            </div>
        )
    };

    load = (type) => {
        const {dispatch} = this.props;
        dispatch({
            type: 'monitor/' + type,
            payload: {
                instanceId: this.props.id
            }
        })
    };
}


export default HttpTrace;
