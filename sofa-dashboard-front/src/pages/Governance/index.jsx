import React from 'react';
import {Divider, Switch, Typography, Icon, Form, Button, Collapse, Statistic, Col, Row, Tag, message, Card, Table, Input, Select, Drawer, List} from 'antd';
import {connect} from 'dva';
import PropertyDetail from "@/pages/Instance/component/PropertyDetail";
const {Option} = Select;
const {Search} = Input;
const {Panel} = Collapse;
const {Text} = Typography;
let formId = 0;

@connect(({governance}) => ({
    list: governance.list,
    providerListData: governance.providerListData || [],
    consumerListData: governance.consumerListData || [],
    sessionServerList: governance.sessionServerList || [],
    sessionHealth: governance.sessionHealth || [],
    metaHealth: governance.metaHealth || [],
    dataHealth: governance.dataHealth || [],
    dataServerList: governance.dataServerList || [],
    metaServerList: governance.metaServerList || [],
    datum: governance.datum || [],
    datumCount: governance.datumCount || [],
    sessionPushSwitch: governance.sessionPushSwitch || [],
    metaPushSwitch: governance.metaPushSwitch || [],
    enableDataDatumExpire: governance.enableDataDatumExpire || [],
    enableDataRenewSnapshot: governance.enableDataRenewSnapshot || [],
    renewEnable: governance.renewEnable || [],
    sessionRenewEnable: governance.sessionRenewEnable || [],
    dataRenewEnable: governance.renewEnable || [],
    peerList: governance.peerList || [],
    registerNodeList: governance.registerNodeList || [],
}))
class Governance extends React.Component {
    state = {
        columns: [
            {
                title: '服务ID',
                dataIndex: 'serviceId',
                key: 'serviceId',
                render: serviceId => <a href={`/governance/details?serviceId=${serviceId}`}>{serviceId}</a>,
            },
            {
                title: '提供此服务的应用',
                dataIndex: 'serviceProviderAppName',
                key: 'serviceProviderAppName',
            },
            {
                title: '服务提供者数目',
                dataIndex: 'serviceProviderAppNum',
                key: 'serviceProviderAppNum',
            },
            {
                title: '服务消费者数目',
                dataIndex: 'serviceConsumerAppNum',
                key: 'serviceConsumerAppNum',
            },
        ],
        queryType: "service",
        visible: false,
        registerNodeVisible: false,
        datumVisible: false,
        serverType: "Session",
        publisherCount: "",
        SubscriberCount: "",
        WatcherCount: "",
        sessionRenewEnable: false,
        dataRenewEnable: false,
        renewEnable: false,
        pushDataEnable: false,
        selectedRowKeys: [],
        selectedRows: [],
        loading: false,
        ipArrayList: []
    };

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetch',
            payload: {
                "queryType": this.state.queryType,
                "queryKey": ""
            }
        });

        dispatch({
            type: 'governance/fetchSessionDataCount',
        }).then((r) => {
            if (r != undefined) {
                this.setState({
                    subscriberCount: r[0],
                    publisherCount: r[1],
                    watcherCount: r[2],
                })
            }
        })
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId);
    }

    showDrawer = (appName) => {
        this.setState({
            visible: true,
        });

        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchServiceByAppName',
            payload: {
                "appName": appName
            }
        });
    };

    showSearchDatumDrawer = (appName) => {
        this.setState({
            datumVisible: true,
        });
    };

    handleSearch = (value) => {
        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetch',
            payload: {
                "queryKey": value,
                "queryType": this.state.queryType
            }
        });
    }

    handleSessionRegisterNode = () => {

        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchRegisterNodeByType',
            payload: {
                "type": "session"
            }
        })
        this.setState({
            registerNodeVisible: true,
        });
    };

    handleMetaRegisterNode = () => {

        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchRegisterNodeByType',
            payload: {
                "type": "meta"
            }
        })
        this.setState({
            registerNodeVisible: true,
        });
    };

    handleDataRegisterNode = () => {

        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchRegisterNodeByType',
            payload: {
                "type": "data"
            }
        })
        this.setState({
            registerNodeVisible: true,
        });
    };

    handleChangePeer = () => {
        this.state.ipArrayList.splice(0, this.state.ipArrayList.length);
        this.state.selectedRows.map(item => {
            this.state.ipArrayList.push(item.ipAddress)
        })
        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchChangePeer',
            payload: {
                "ipAddressList": this.state.ipArrayList.join(',')
            }
        }).then(res=>{
            if(res!=undefined){
                if(res.success==true){
                    message.success(res.message)
                }else{
                    message.error(res.message)
                }
            }
        })
    };

    handleResetPeer = () => {
        this.state.ipArrayList.splice(0, this.state.ipArrayList.length);
        this.state.selectedRows.map(item => {
            this.state.ipArrayList.push(item.ipAddress)
        })
        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchResetPeer',
            payload: {
                "ipAddressList": this.state.ipArrayList.join(',')
            }
        }).then(res=>{
            if(res!=undefined){
                if(res.success==true){
                    message.success(res.message)
                }else{
                    message.error(res.message)
                }
            }
        })
    };

    handleRemovePeer = () => {
        this.state.ipArrayList.splice(0, this.state.ipArrayList.length);
        this.state.selectedRows.map(item => {
            this.state.ipArrayList.push(item.ipAddress)
        })
        const {dispatch} = this.props;
        dispatch({
            type: 'governance/fetchRemovePeer',
            payload: {
                "ipAddressList": this.state.ipArrayList.join(',')
            }
        }).then(res=>{
            if(res!=undefined){
                if(res.success==true){
                    message.success(res.message)
                }else{
                    message.error(res.message)
                }
            }
        })
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    onRegisterNodeClose = () => {
        this.setState({
            registerNodeVisible: false,
        });
    };
    onSearchDatumClose = () => {
        this.setState({
            datumVisible: false,
        });
    };

    onSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({selectedRowKeys, selectedRows});
    };

    onSessionSwitchChange = () => {
        const {dispatch} = this.props;
        this.setState({
            sessionRenewEnable: !this.state.sessionRenewEnable
        })
        dispatch({
            type: 'governance/fetchSessionRenew',
            payload: {
                enable: !this.state.sessionRenewEnable
            }
        });

    }

    onDataSwitchChange = () => {
        const {dispatch} = this.props;
        this.setState({
            dataRenewEnable: !this.state.dataRenewEnable
        })
        dispatch({
            type: 'governance/fetchDataRenew',
            payload: {
                enable: !this.state.dataRenewEnable
            }
        });

    }

    onRenewSwitchChange = () => {
        const {dispatch} = this.props;
        this.setState({
            renewEnable: !this.state.renewEnable
        })
        dispatch({
            type: 'governance/fetchRenew',
            payload: {
                enable: !this.state.renewEnable
            }
        });
    }
    onPushDataSwitchChange = () => {
        const {dispatch} = this.props;
        this.setState({
            pushDataEnable: !this.state.pushDataEnable
        })
        dispatch({
            type: 'governance/fetchStopPushDataSwitch',
            payload: {
                type: !this.state.pushDataEnable
            }
        });
    }

    CollapseCallback = (key) => {
        const {dispatch} = this.props;
        if (key === 'health') {
            dispatch({
                type: 'governance/fetchHealth',
            })
        } else if (key == 'serverList') {
            dispatch({
                type: 'governance/fetchServerListAll',
            });
        } else if (key == 'searchDatum') {
            dispatch({
                type: 'governance/fetchDatumCount',
            });
        } else if (key == 'pushSwitch') {
            const intervalId = setInterval(() => {
                const {dispatch} = this.props;
                dispatch({
                    type: 'governance/fetchPushSwitch',
                })
                dispatch({
                    type: 'governance/fetchRenewPushSwitch',
                })
            }, 2000);
            this.setState({intervalId: intervalId})
        } else if (key == 'peer') {
            dispatch({
                type: 'governance/fetchPeerList',
            });
        } else if (key == 'persistentData') {

        } else if (key == 'registerNode') {

        } else if (key == 'blackList') {

        }
    }

    render() {
        const appColumns = [
            {
                title: '应用名',
                dataIndex: 'appName',
                key: 'appName',
                render: appName => <a onClick={() => this.showDrawer(appName)}>{appName}</a>,
            },
        ];

        const serviceColumns = [
            {
                title: '服务ID',
                dataIndex: 'serviceId',
                key: 'serviceId',
                render: serviceId => <a href={`/governance/details?serviceId=${serviceId}`}>{serviceId}</a>,
            },
            {
                title: '提供此服务的应用',
                dataIndex: 'serviceProviderAppName',
                key: 'serviceProviderAppName',
            },
            {
                title: '服务提供者数目',
                dataIndex: 'serviceProviderAppNum',
                key: 'serviceProviderAppNum',
            },
            {
                title: '服务消费者数目',
                dataIndex: 'serviceConsumerAppNum',
                key: 'serviceConsumerAppNum',
            },
        ];

        const handleChange = (value) => {
            const {dispatch} = this.props;
            if (value === 'app') {
                this.setState({
                    columns: appColumns,
                    queryType: "app"
                });
                dispatch({
                    type: 'governance/fetch',
                    payload: {
                        "queryType": "app",
                        "queryKey": ""
                    }
                });

            } else {
                this.setState({
                    columns: serviceColumns,
                    queryType: "service"
                });
                dispatch({
                    type: 'governance/fetch',
                    payload: {
                        "queryType": "service",
                        "queryKey": ""
                    }
                });
            }
        }

        const peerColumns = [
            {
                title: 'IpAddress',
                dataIndex: 'ipAddress',
            }
        ];
        const {selectedRowKeys} = this.state;

        const hasSelected = selectedRowKeys.length > 0

        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        return (
            < div>
                <Row>
                    <Col span={6} push={18}>
                        <div style={{marginBottom: 15, marginTop: -15, marginLeft: 15}}
                             className="card-container">
                            <div style={{marginBottom: 30}}>
                                <Collapse
                                    bordered={false}
                                    accordion
                                    onChange={this.CollapseCallback}
                                    expandIconPosition="left"
                                >
                                    <Panel header="Health" key="health">
                                        <Divider orientation="left">Session Server Health
                                            <Tag style={{marginLeft: 10}}
                                                 color={this.props.sessionHealth.success === true ? 'green' : 'red'}>{this.props.sessionHealth.success === true ? 'success' : 'fail'}</Tag>
                                        </Divider>
                                        <Text>{this.props.sessionHealth.message}</Text>
                                        <Divider orientation="left">Meta Server Health
                                            <Tag style={{marginLeft: 10}}
                                                 color={this.props.sessionHealth.success === true ? 'green' : 'red'}>{this.props.metaHealth.success === true ? 'success' : 'fail'}</Tag>
                                        </Divider>
                                        <Text>{this.props.metaHealth.message}</Text>
                                        <Divider orientation="left">Data Server Health
                                            <Tag style={{marginLeft: 10}}
                                                 color={this.props.sessionHealth.success === true ? 'green' : 'red'}>{this.props.dataHealth.success === true ? 'success' : 'fail'}</Tag>
                                        </Divider>
                                        <Text>{this.props.dataHealth.message}</Text>
                                    </Panel>
                                    <Panel header="ServerList" key="serverList">
                                        <Card title="SessionServer">
                                            <List
                                                size="small"
                                                dataSource={this.props.sessionServerList}
                                                style={{minHeight: 30}}
                                                renderItem={item => < List.Item>{item}</List.Item>}
                                            />
                                        </Card>
                                        <Card title="DataServer">
                                            <List
                                                size="small"
                                                dataSource={this.props.dataServerList}
                                                style={{minHeight: 30}}
                                                renderItem={item => < List.Item>{item}</List.Item>}
                                            />
                                        </Card>
                                        <Card title="MetaServer">
                                            <List
                                                size="small"
                                                dataSource={this.props.metaServerList}
                                                style={{minHeight: 30}}
                                                renderItem={item => < List.Item>{item}</List.Item>}
                                            />
                                        </Card>
                                    </Panel>
                                    <Panel header="Search Datum" key="searchDatum">
                                        <WrappedSearchDatumForm/>
                                        <p>
                                            {this.props.datumCount}
                                        </p>
                                        <Button onClick={this.showSearchDatumDrawer} type="primary">
                                            查看
                                        </Button>
                                        <Drawer
                                            title="详情"
                                            placement="right"
                                            closable={false}
                                            onClose={this.onSearchDatumClose}
                                            visible={this.state.datumVisible}
                                            width={640}
                                        >
                                            <div>
                                                <PropertyDetail
                                                    title="Datum"
                                                    data={this.props.datum === undefined ? [] : this.props.datum}/>
                                            </div>
                                        </Drawer>
                                    </Panel>
                                    <Panel header="Push Switch" key="pushSwitch">
                                        <Divider orientation="left">pushSwitch</Divider>
                                        <Row gutter={[8, 24]}>
                                            <Col span={8}>
                                                <Switch checkedChildren="open" unCheckedChildren="close"
                                                        checked={this.state.pushDataEnable}
                                                        onChange={this.onPushDataSwitchChange}/>
                                                <Text strong={true}> Switch</Text>
                                            </Col>
                                            <Col span={8}>
                                                <Switch checkedChildren="open" unCheckedChildren="close"
                                                        checked={this.props.sessionPushSwitch === 'open' ? true : false}
                                                        disabled={true}/>
                                                <Text strong={true}> Session</Text>
                                            </Col>
                                            <Col span={8}>
                                                <Switch checkedChildren="open" unCheckedChildren="close"
                                                        checked={this.props.metaPushSwitch === 'open' ? true : false}
                                                        disabled={true}/>
                                                <Text strong={true}> Meta</Text>
                                            </Col>
                                        </Row>


                                        <Divider orientation="left">Renew Switch</Divider>

                                        <Row gutter={[8, 24]}>
                                            <Col span={8}>
                                                <Switch checkedChildren="able" unCheckedChildren="disable"
                                                        checked={this.state.renewEnable}
                                                        onChange={this.onRenewSwitchChange}/>
                                                <Text strong={true}> renewEnable</Text>
                                            </Col>
                                            <Col span={8}>
                                                <Switch checkedChildren="able" unCheckedChildren="disable"
                                                        checked={this.state.sessionRenewEnable}
                                                        onChange={this.onSessionSwitchChange}/>
                                                <Text strong={true}> sessionPushSwitch</Text>
                                            </Col>
                                            <Col span={8}>
                                                <Switch checkedChildren="able" unCheckedChildren="disable"
                                                        checked={this.state.dataRenewEnable}
                                                        onChange={this.onDataSwitchChange}/>
                                                <Text strong={true}> dataRenewEnable</Text>
                                            </Col>
                                        </Row>

                                        <div style={{marginTop: 30}}>
                                            <Row gutter={[8, 24]}>
                                                <Col span={12}>
                                                    <Switch checkedChildren="open" unCheckedChildren="close"
                                                            checked={this.props.enableDataDatumExpire === 'true' ? true : false}
                                                            disabled={true}/>
                                                    <Text strong={true}> DatumExpire</Text>
                                                </Col>
                                                <Col span={12}>
                                                    <Switch checkedChildren="open" unCheckedChildren="close"
                                                            checked={this.props.enableDataRenewSnapshot === 'true' ? true : false}
                                                            disabled={true}/>
                                                    <Text strong={true}> RenewSnapshot</Text>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Panel>
                                    <Panel header="Peer" key="peer">

                                        <div style={{marginTop: 8}}>
                                            <Row gutter={[24]}>
                                                <Col span={8}>
                                                    <Button type="primary" onClick={this.handleChangePeer}>
                                                        Change
                                                    </Button>
                                                </Col>
                                                <Col span={8}>
                                                    <Button type="primary" onClick={this.handleResetPeer}>
                                                        Reset
                                                    </Button>
                                                </Col>
                                                <Col span={8}>
                                                    <Button type="danger" onClick={this.handleRemovePeer}>
                                                        Remove
                                                    </Button>
                                                </Col>
                                            </Row>
                                            <span style={{marginTop: 8}}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
                                        </div>
                                        <Table rowSelection={rowSelection}
                                               columns={peerColumns}
                                               dataSource={this.props.peerList}
                                               rowKey={record => record.ipAddress}/>
                                    </Panel>
                                    <Panel header="PersistentData" key="persistentData">
                                        <WrappedPersistentForm/>
                                    </Panel>
                                    <Panel header="RegisterNode" key="registerNode">
                                        <div style={{marginTop: 8}}>
                                            <Row gutter={[24]}>
                                                <Col span={8}>
                                                    <Button type="primary" onClick={this.handleSessionRegisterNode}>
                                                        Session
                                                    </Button>
                                                </Col>
                                                <Col span={8}>
                                                    <Button type="primary" onClick={this.handleMetaRegisterNode}>
                                                        Meta
                                                    </Button>
                                                </Col>
                                                <Col span={8}>
                                                    <Button type="primary" onClick={this.handleDataRegisterNode}>
                                                        Data
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </div>
                                        <Drawer
                                            title="RegisterNode"
                                            placement="right"
                                            closable={false}
                                            onClose={this.onRegisterNodeClose}
                                            visible={this.state.registerNodeVisible}
                                            width={640}
                                        >
                                            <div>
                                                <PropertyDetail
                                                    title="详情"
                                                    data={this.props.registerNodeList === undefined ? [] : this.props.registerNodeList}/>
                                            </div>
                                        </Drawer>
                                    </Panel>
                                    <Panel header="BlackList" key="blackList">
                                        <WrappedBlackListForm/>
                                    </Panel>
                                </Collapse>
                            </div>
                        </div>
                    </Col>
                    <Col span={18} pull={6}>
                        <Card bordered={false} hoverable={false}
                              style={{marginBottom: 15, marginTop: -15, marginLeft: -15}}>
                            <Select defaultValue="service" style={{width: 120, marginRight: 10}}
                                    onChange={handleChange}>
                                <Option value="service">服务维度</Option>
                                <Option value="app">应用维度</Option>
                            </Select>
                            <Search placeholder="please input query keyword"
                                    onSearch={value => this.handleSearch(value)} enterButton
                                    style={{width: 500, marginBottom: 20}}/>
                            <div className="site-card-wrapper">
                                <Row gutter={16}>
                                    <Col span={8}>
                                        <Card bordered={false}>
                                            <Statistic title="Publisher" value={this.state.publisherCount}/>
                                        </Card>
                                    </Col>
                                    <Col span={8}>
                                        <Card bordered={false}>
                                            <Statistic title="Subscriber" value={this.state.subscriberCount}/>
                                        </Card>
                                    </Col>
                                    <Col span={8}>
                                        <Card bordered={false}>
                                            <Statistic title="Watcher" value={this.state.watcherCount}/>
                                        </Card>
                                    </Col>
                                </Row>
                            </div>
                            <Table
                                dataSource={this.props.list}
                                columns={this.state.columns}
                                rowKey={record => (record.appName != undefined) ? record.appName : record.serviceId}
                            />
                            <Drawer
                                title="服务详情"
                                placement="right"
                                closable={false}
                                onClose={this.onClose}
                                visible={this.state.visible}
                                width={640}
                            >
                                <Card title="服务发布列表">
                                    <List
                                        size="small"
                                        dataSource={this.props.providerListData}
                                        style={{minHeight: 30}}
                                        renderItem={item => <List.Item>{item}</List.Item>}
                                    />
                                </Card>

                                <Card title="服务消费列表" style={{marginTop: 10}}>
                                    <List
                                        size="small"
                                        dataSource={this.props.consumerListData}
                                        style={{minHeight: 30}}
                                        renderItem={item => <List.Item>{item}</List.Item>}
                                    />
                                </Card>
                            </Drawer>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
};

@connect(({governance})=>({
    datum:governance.datum||[],
}))
class SearchDatumForm extends React.Component {
    state = {
        visible: false,
    }

    onClose = () => {
        this.setState({
            visible: false,
        });
    };


    componentDidMount() {
        // To disable submit button at the beginning.
        this.props.form.validateFields();
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const {dispatch} = this.props;

                dispatch({
                    type: 'governance/fetchDatumByDataInfoId',
                    payload: {
                        "dataId": values.dataId === undefined ? '' : values.dataId,
                        "group": values.group === undefined ? '' : values.group,
                        "instanceId": values.instanceId === undefined ? '' : values.instanceId,
                        "dataCenter": values.dataCenter === undefined ? '' : values.dataCenter,
                    }
                }).then(res=>{
                    if(res!=undefined){
                       message.success()
                    }
                })
            }
        });
    };

    render() {
        const {getFieldDecorator} = this.props.form;


        return (
            <div>
                <Form labelAlign="left" layout="vertical" onSubmit={this.handleSubmit}>
                    <Form.Item label="dataId">
                        {getFieldDecorator('dataId')(
                            <Input
                                placeholder="DataId"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="group">
                        {getFieldDecorator('group')(
                            <Input
                                placeholder="Group"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="instanceId"
                    >
                        {getFieldDecorator('instanceId')(
                            <Input
                                placeholder="InstanceId"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="dataCenter">
                        {getFieldDecorator('dataCenter')(
                            <Input
                                placeholder="DataCenter"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>

                        <Button htmlType="submit" type="primary">
                            搜索
                        </Button>

                    </Form.Item>
                </Form>
            </div>


        );
    }
}

@connect()
class PersistentForm extends React.Component {
    state = {
        visible: false
    }

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    componentDidMount() {
        // To disable submit button at the beginning.
        this.props.form.validateFields();
    }


    removePersistentData = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            const {dispatch} = this.props;

            dispatch({
                type: 'governance/fetchPersistentDataRemove',
                payload: {
                    "dataId": values.dataId,
                    "group": values.group,
                    "instanceId": values.instanceId,
                    "version": values.version,
                    "data": values.data,
                }
            }).then((r) => {
                if (r != undefined) {

                    if (r.success === true) {
                        message.success("remove success")
                    } else {
                        message.error("remove error:" + r.message)
                    }
                }

            })
        });
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                const {dispatch} = this.props;

                dispatch({
                    type: 'governance/fetchPersistentDataPut',
                    payload: {
                        "dataId": values.dataId,
                        "group": values.group,
                        "instanceId": values.instanceId,
                        "version": values.version,
                        "data": values.data,
                    }
                }).then((r) => {
                    if (r != undefined) {

                        if (r.success === true) {
                            message.success("put success")
                        } else {
                            message.error("put error:" + r.message)
                        }
                    }

                })

            }
        });
    };

    render() {
        const {getFieldDecorator} = this.props.form;


        return (
            <div>

                <Form labelAlign="left" layout="vertical" onSubmit={this.handleSubmit}>
                    <Form.Item label="dataId">
                        {getFieldDecorator('dataId')(
                            <Input
                                placeholder="DataId"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="group">
                        {getFieldDecorator('group')(
                            <Input
                                placeholder="Group"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="instanceId">
                        {getFieldDecorator('instanceId')(
                            <Input
                                placeholder="InstanceId"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="version">
                        {getFieldDecorator('version')(
                            <Input
                                placeholder="Version"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item label="data">
                        {getFieldDecorator('data')(
                            <Input
                                placeholder="Data"
                            />,
                        )}
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Put
                        </Button>
                    </Form.Item>

                    <Form.Item>
                        <Button onClick={this.removePersistentData} type="danger">
                            Remove
                        </Button>
                    </Form.Item>


                </Form>
            </div>


        );
    }
}

@connect()
class BlackListForm extends React.Component {
    state = {
        pubIpList: []
    }

    remove = k => {
        const {form} = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        // We need at least one passenger
        if (keys.length === 1) {
            return;
        }

        // can use data-binding to set
        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    };

    add = () => {
        const {form} = this.props;
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(formId++);
        form.setFieldsValue({
            keys: nextKeys,
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        const {dispatch} = this.props;
        this.state.pubIpList.splice(0, this.state.pubIpList.length);
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const {keys, pubIp} = values;
                keys.map(key => {
                    this.state.pubIpList.push(pubIp[key])
                })
                dispatch({
                    type: 'governance/fetchBlacklistPush',
                    payload: {
                        "pubIpList": this.state.pubIpList.join(','),
                        "subPrefix": values.subPrefix
                    }
                }).then(res => {
                    if (res != undefined) {
                        if(res.success===true){
                            message.success(res.message)
                        }else{
                            message.error(res.message)
                        }
                    }
                })
            }
        });
    };
    0

    render() {
        const {getFieldDecorator, getFieldValue} = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 4},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 20},
            },
        };
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                xs: {span: 24, offset: 0},
                sm: {span: 20, offset: 4},
            },
        };
        getFieldDecorator('keys', {initialValue: []});
        const keys = getFieldValue('keys');
        const formItems = keys.map((k, index) => (

            <Form.Item
                {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
                label={index === 0 ? 'PUB_IP:' : ''}
                required={false}
                key={k}
            >
                {getFieldDecorator(`pubIp[${k}]`, {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                        {
                            required: true,
                            whitespace: true,
                            message: "Please input passenger's name or delete this field.",
                        },
                    ],
                })(<Input placeholder="passenger name" style={{width: '60%', marginLeft: 6}}/>)}
                {keys.length > 1 ? (
                    <Icon
                        className="dynamic-delete-button"
                        type="minus-circle-o"
                        onClick={() => this.remove(k)}
                    />
                ) : null}
            </Form.Item>


        ));
        return (
            <Form onSubmit={this.handleSubmit}>

                {formItems}
                <Form.Item {...formItemLayoutWithOutLabel}
                           key={'add'}>
                    <Button type="dashed" onClick={this.add} style={{width: '60%'}}>
                        <Icon type="plus"/> Add Pub Ip
                    </Button>

                </Form.Item>
                <Form.Item
                    {...(formItemLayout)}
                    label={'SUB_IP'}
                    key={'subPrefix'}>
                    {getFieldDecorator('subPrefix')(
                        <Input style={{width: '60%', marginLeft: 6}}
                               placeholder="Prefix"
                        />,
                    )}
                </Form.Item>
                <Form.Item {...formItemLayoutWithOutLabel}
                           key={'button'}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedBlackListForm = Form.create({name: 'blackList_form_item'})(BlackListForm);
const WrappedSearchDatumForm = Form.create({name: 'search_datum_form_item'})(SearchDatumForm);
const WrappedPersistentForm = Form.create({name: 'persistent_form_item'})(PersistentForm);
export default Governance;

// 服务治理页，按照应用维度查看，当点击应用名时，右侧抽屉面板展示，问题是，目前从后端获取到的数据没法展示。