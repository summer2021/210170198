package com.alipay.sofa.dashboard.model;


import java.util.List;


public class HttpTraceVO {
    private String timestamp;
    private String method;
    private String url;
    private Integer status;
    private String date;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public static  class BizChartsObject{
        private String bizStatus;
        private Integer bizCount;
        private String bizDate;

        public String getBizStatus() {
            return bizStatus;
        }

        public void setBizStatus(String bizStatus) {
            this.bizStatus = bizStatus;
        }

        public Integer getBizCount() {
            return bizCount;
        }

        public void setBizCount(Integer bizCount) {
            this.bizCount = bizCount;
        }

        public String getBizDate() {
            return bizDate;
        }

        public void setBizDate(String bizDate) {
            this.bizDate = bizDate;
        }
    }
}
