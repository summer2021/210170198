import { stringify } from 'qs';
import request from '../utils/request';

export async function queryAll(params) {
    if (params.queryType === 'app') {
        return request(`/api/service/all-app?query=` + params.queryKey);
    } else {
        return request(`/api/service/all-service?query=` + params.queryKey);
    }
}

export async function queryServiceByAppName(params) {
    return request(`/api/service/service-app?${stringify(params)}`);
}

// 获取服务发布详情
export async function queryProviderDetails(params) {
    return request(`/api/service/query/providers?${stringify(params)}`);
}

// 获取服务消费详情
export async function queryConsumerDetails(params) {
    return request(`/api/service/query/consumers?${stringify(params)}`);
}

export async function getPushSwitch() {
    return request(`/api/service/pushSwitch`);
}

export async function getSessionDataCount() {
    return request(`/api/service/data/count`);
}

export async function checkHealth() {
    return request(`/api/service/health/check`);
}

export async function queryServerListAll() {
    return request(`/api/service/serverList/query`);
}

export async function getDatumByDataInfoId(params) {
    return request(`/api/service/datum/query?${stringify(params)}`);
}

export async function getDatumCount() {
    return request(`/api/service/datum/count`);
}

export async function changeDataRenew(params) {
    return request(`/api/service/renewSwitch/data/change?${stringify(params)}`);
}

export async function changeSessionRenew(params) {
    return request(`/api/service/renewSwitch/session/change?${stringify(params)}`);
}

export async function changeRenew(params) {
    return request(`/api/service/renewSwitch/renew/change?${stringify(params)}`);
}

export async function getRenewSwitch() {
    return request(`/api/service/renewSwitch/get`);
}

export async function changeStopPushDataSwitch(params) {
    return request(`/api/service/stopPushDataSwitch/`+params.type);
}

export async function queryChangePeer(params) {
    return request(`/api/service/manage/changePeer?${stringify(params)}`);
}

export async function queryResetPeer(params) {
    return request(`/api/service/manage/resetPeer?${stringify(params)}`);
}

export async function queryRemovePeer(params) {
    return request(`/api/service/manage/removePeer?${stringify(params)}`);
}

export async function queryPeerList() {
    return request(`/api/service/manage/queryPeer`);
}

export async function queryPersistentDataPut(params) {
    return request(`/api/service/persistentData/put?${stringify(params)}`);
}

export async function queryPersistentDataRemove(params) {
    return request(`/api/service/persistentData/remove?${stringify(params)}`);
}

export async function queryRegisterNodeByType(params) {
    return request(`/api/service/`+params.type+`/node/query`);
}

export async function queryBlacklistPush(params) {
    return request(`/api/service/blacklist/update?${stringify(params)}`);
}